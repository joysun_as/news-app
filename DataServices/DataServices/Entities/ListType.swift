//
//  ListType.swift
//  DataServices
//
//  Created by DNR on 27/03/22.
//

import Foundation

public enum ListType {
    case search
    case headline
    case source
}
