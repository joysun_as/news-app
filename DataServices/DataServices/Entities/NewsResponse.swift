//
//  NewsResponse.swift
//  DataServices
//
//  Created by DNR on 26/03/22.
//

import Foundation

// MARK: - NewsResponse
public struct NewsResponse: Codable {
    public var status: String?
    public var totalResults: Int?
    public var articles: [Article]?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case totalResults = "totalResults"
        case articles = "articles"
    }

    public init(status: String?, totalResults: Int?, articles: [Article]?) {
        self.status = status
        self.totalResults = totalResults
        self.articles = articles
    }
}

// MARK: - Article
public struct Article: Codable {
    public var source: Source?
    public var author: String?
    public var title: String?
    public var articleDescription: String?
    public var url: String?
    public var urlToImage: String?
    public var publishedAt: String?
    public var content: String?

    enum CodingKeys: String, CodingKey {
        case source = "source"
        case author = "author"
        case title = "title"
        case articleDescription = "description"
        case url = "url"
        case urlToImage = "urlToImage"
        case publishedAt = "publishedAt"
        case content = "content"
    }

    public init(source: Source?, author: String?, title: String?, articleDescription: String?, url: String?, urlToImage: String?, publishedAt: String?, content: String?) {
        self.source = source
        self.author = author
        self.title = title
        self.articleDescription = articleDescription
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
    }
}

// MARK: - Source
public struct Source: Codable {
    public var id: String?
    public var name: String?
    public var sourceDescription: String?
    public var url: String?
    public var category: String?
    public var language: String?
    public var country: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case sourceDescription = "description"
        case url = "url"
        case category = "category"
        case language = "language"
        case country = "country"
    }

    public init(id: String?, name: String?, sourceDescription: String?, url: String?, category: String?, language: String?, country: String?) {
        self.id = id
        self.name = name
        self.sourceDescription = sourceDescription
        self.url = url
        self.category = category
        self.language = language
        self.country = country
    }
}
