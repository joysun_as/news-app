//
//  ApiEndPoint.swift
//  DataServices
//
//  Created by DNR on 26/03/22.
//

import Foundation

public enum ApiEndPoint : String {
    case baseURL = "https://newsapi.org/v2/"
    case headlinesPath = "top-headlines/"
    case everythingPath = "everything"
//    case key = "e767db00e20e4aaea8bf05eff6533545"
    case key = "9d9d719c8a534afba2bf4c054ee87fb4"
    case apiKeyPath = "apiKey"
    case countryPath = "country"
    case indonesiaKey = "id"
    case categoryPath = "category"
    case pagePath = "page"
    case pageSizePath = "pageSize"
    case searchPath = "q"
    case sourcePath = "sources"
}
