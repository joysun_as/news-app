//
//  NewsServiceProtocol.swift
//  DataServices
//
//  Created by DNR on 26/03/22.
//

import Foundation

public protocol NewsServiceProtocol {
    func getNewsHeadlineList( page : Int, pageSize : Int, completion : @escaping(NewsResponse)->Void)
    func searchNews(page : Int, pageSize : Int, search : String, completion : @escaping(NewsResponse)->Void)
    func getNewsByCategory(page : Int, pageSize : Int, category : String, completion : @escaping(NewsResponse)->Void)
    func getSourceList(page : Int, pageSize : Int, category : String, completion : @escaping([Source])->Void)
    func getNewsBySource(page : Int, pageSize : Int, source : String, completion : @escaping(NewsResponse)->Void)
}
