//
//  NewsService.swift
//  DataServices
//
//  Created by DNR on 26/03/22.
//

import Foundation
import RxAlamofire
import RxSwift
import SwiftyJSON


public class NewsService : NewsServiceProtocol{
    public init(){
    }
    
    public var disposeBag = DisposeBag()
    
    
    public func getNewsHeadlineList(page: Int, pageSize: Int, completion: @escaping (NewsResponse) -> Void) {
       
        let baseURL = ApiEndPoint.baseURL.rawValue
        let headlines = ApiEndPoint.headlinesPath.rawValue
        let apiKey = ApiEndPoint.apiKeyPath.rawValue
        let key = ApiEndPoint.key.rawValue
        let pagePath = ApiEndPoint.pagePath.rawValue
        let pageSizePath = ApiEndPoint.pageSizePath.rawValue
        let countryPath = ApiEndPoint.countryPath.rawValue
        let indonesiaKey = ApiEndPoint.indonesiaKey.rawValue
        let endpointURL = baseURL + headlines
        
        let parameter = [apiKey : key, pagePath : page, pageSizePath : pageSize, countryPath : indonesiaKey] as [String : Any]
        
        RxAlamofire.requestJSON(.get, endpointURL, parameters: parameter).subscribe(onNext: { (r, value) in
            let json = JSON(value)
            if r.statusCode == 200{
                let newsResponse = try! JSONDecoder().decode(NewsResponse.self, from: json.rawData())
                completion(newsResponse)
            }
        }, onError: {(error) in
            print(error.localizedDescription)

        }).disposed(by: disposeBag)
    }
    
    public func searchNews(page: Int, pageSize: Int, search: String, completion: @escaping (NewsResponse) -> Void) {
        
        let baseURL = ApiEndPoint.baseURL.rawValue
        let everythingPath = ApiEndPoint.everythingPath.rawValue
        let apiKey = ApiEndPoint.apiKeyPath.rawValue
        let key = ApiEndPoint.key.rawValue
        let pagePath = ApiEndPoint.pagePath.rawValue
        let pageSizePath = ApiEndPoint.pageSizePath.rawValue
        let searchPath = ApiEndPoint.searchPath.rawValue
        let endpointURL = baseURL + everythingPath
        
        let parameter = [apiKey : key, pagePath : page, pageSizePath : pageSize, searchPath : search] as [String : Any]
        
        RxAlamofire.requestJSON(.get, endpointURL, parameters: parameter).subscribe(onNext: { (r, value) in
            let json = JSON(value)
            if r.statusCode == 200{
                let newsResponse = try! JSONDecoder().decode(NewsResponse.self, from: json.rawData())
                completion(newsResponse)
            }
        }, onError: {(error) in
            print(error.localizedDescription)

        }).disposed(by: disposeBag)
        
    }
    
    public func getNewsByCategory(page: Int, pageSize: Int, category: String, completion: @escaping (NewsResponse) -> Void) {
        
        let baseURL = ApiEndPoint.baseURL.rawValue
        let headlines = ApiEndPoint.headlinesPath.rawValue
        let apiKey = ApiEndPoint.apiKeyPath.rawValue
        let key = ApiEndPoint.key.rawValue
        let pagePath = ApiEndPoint.pagePath.rawValue
        let pageSizePath = ApiEndPoint.pageSizePath.rawValue
        let categoryPath = ApiEndPoint.categoryPath.rawValue
        let countryPath = ApiEndPoint.countryPath.rawValue
        let indonesiaKey = ApiEndPoint.indonesiaKey.rawValue
        let endpointURL = baseURL + headlines
        
        let parameter = [apiKey : key, pagePath : page, pageSizePath : pageSize, categoryPath : category, countryPath : indonesiaKey] as [String : Any]
        
        RxAlamofire.requestJSON(.get, endpointURL, parameters: parameter).subscribe(onNext: { (r, value) in
            let json = JSON(value)
            if r.statusCode == 200{
                let newsResponse = try! JSONDecoder().decode(NewsResponse.self, from: json.rawData())
                completion(newsResponse)
            }
        }, onError: {(error) in
            print(error.localizedDescription)

        }).disposed(by: disposeBag)
        
    }
    
    public func getSourceList(page: Int, pageSize: Int, category: String, completion: @escaping ([Source]) -> Void) {
        let baseURL = ApiEndPoint.baseURL.rawValue
        let headlines = ApiEndPoint.headlinesPath.rawValue
        let apiKey = ApiEndPoint.apiKeyPath.rawValue
        let key = ApiEndPoint.key.rawValue
        let pagePath = ApiEndPoint.pagePath.rawValue
        let pageSizePath = ApiEndPoint.pageSizePath.rawValue
        let categoryPath = ApiEndPoint.categoryPath.rawValue
        let sourcePath = ApiEndPoint.sourcePath.rawValue
        let endpointURL = baseURL + headlines + sourcePath
        
        
        let parameter = [apiKey : key, pagePath : page, pageSizePath : pageSize, categoryPath : category] as [String : Any]
        RxAlamofire.requestJSON(.get, endpointURL, parameters: parameter).subscribe(onNext: { (r, value) in
            let json = JSON(value)
            if r.statusCode == 200{
                let newsResponse = try! JSONDecoder().decode([Source].self, from: json["sources"].rawData())
                completion(newsResponse)
            }
        }, onError: {(error) in
            print(error.localizedDescription)

        }).disposed(by: disposeBag)
        
    }
    
    public func getNewsBySource(page: Int, pageSize: Int, source: String, completion: @escaping (NewsResponse) -> Void) {
        let baseURL = ApiEndPoint.baseURL.rawValue
        let headlines = ApiEndPoint.headlinesPath.rawValue
        let apiKey = ApiEndPoint.apiKeyPath.rawValue
        let key = ApiEndPoint.key.rawValue
        let pagePath = ApiEndPoint.pagePath.rawValue
        let pageSizePath = ApiEndPoint.pageSizePath.rawValue
        let sourcePath = ApiEndPoint.sourcePath.rawValue
        let endpointURL = baseURL + headlines
        
        let parameter = [apiKey : key, pagePath : page, pageSizePath : pageSize, sourcePath : source] as [String : Any]
        RxAlamofire.requestJSON(.get, endpointURL, parameters: parameter).subscribe(onNext: { (r, value) in
            let json = JSON(value)
            if r.statusCode == 200{
                let newsResponse = try! JSONDecoder().decode(NewsResponse.self, from: json.rawData())
                completion(newsResponse)
            }
        }, onError: {(error) in
            print(error.localizedDescription)

        }).disposed(by: disposeBag)
    }
    
    
}
