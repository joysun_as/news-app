//
//  CardView.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit

@IBDesignable class CardView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 2
    
    @IBInspectable var shadowOfSetWidth : CGFloat = 0
    
    @IBInspectable var shadowOfSetHeight : CGFloat = 5
    
    @IBInspectable var shadowColor : UIColor = UIColor.black
    
    @IBInspectable var shadowOpacity : CGFloat = 0.5
    
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        
        layer.shadowColor = shadowColor.cgColor
        
        layer.shadowOffset = CGSize(width: shadowOfSetWidth, height: shadowOfSetHeight)
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.shadowPath = shadowPath.cgPath
        
        layer.shadowOpacity = Float(shadowOpacity)
    }

}
