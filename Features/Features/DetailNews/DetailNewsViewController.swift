//
//  DetailNewsViewController.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import WebKit

class DetailNewsViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var url : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newsURL = URL (string: url)
                let requestObj = URLRequest(url: newsURL!)
        webView.load(requestObj)
    }

}
