//
//  HomeProtocols.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import Foundation
import DataServices

protocol HomeViewProtocol : AnyObject {
    var presenter : HomePresenterProtocol?{get set}
    
    func showHeadlines(result : NewsResponse)
    func showProgress()
    func hideProgress()
}

protocol HomePresenterProtocol : AnyObject {
    var view : HomeViewProtocol? {get set}
    var interactor : HomeInteractorProtocol? {get set}
    var router : HomeRouterProtocol? {get set}
    
    func getHeadlines(page : Int, pageSize : Int);
    func getSelectedCategory(category : String)
    func getSelectedHeadlines(url : String)
    func getNewsSearch(search : String)
    func getMoreHeadlines()
}

protocol HomeInteractorProtocol : AnyObject {
    var interactorOutput : HomeInteractorOutputProtocol? {get set}
    var service : NewsServiceProtocol? {get set}
    
    func fetchHeadlines(page : Int, pageSize : Int)
    
}

protocol HomeInteractorOutputProtocol : AnyObject {
    func completeFetchHeadlines(result : NewsResponse)
    
}

protocol HomeRouterProtocol : AnyObject {
    func navToDetailNews(url : String)
    func navToNewsList(type : ListType, identifier : String)
    func navToSourceList(category : String)
}
