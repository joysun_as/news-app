//
//  HomeRouter.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import DataServices

class HomeRouter: HomeRouterProtocol {
    
    let homeVC : UIViewController
    
    init(viewcontroller : UIViewController) {
        homeVC = viewcontroller
    }
    
    
    func navToDetailNews(url: String) {
        let detailVC = DetailNewsViewController(nibName: "DetailNewsViewController", bundle: Bundle(for: DetailNewsViewController.self))
        detailVC.url = url
        homeVC.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func navToNewsList(type : ListType, identifier: String) {
        let newsListVC = NewsListRouter.initModule()
        switch type {
        case .search:
            newsListVC.selectedType = .search
            break
        case .headline:
            newsListVC.selectedType = .headline
            break
        default :
            break
        
        }
        newsListVC.identifier = identifier
        newsListVC.navigationItem.title = identifier
        homeVC.navigationController?.pushViewController(newsListVC, animated: true)
    }
    
    func navToSourceList(category: String) {
        let sourceVC = SourceRouter.initModule()
        sourceVC.category = category
        sourceVC.navigationItem.title = category + " Source"
        homeVC.navigationController?.pushViewController(sourceVC, animated: true)
    }
}
