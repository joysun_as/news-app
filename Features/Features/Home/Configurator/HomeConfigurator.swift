//
//  HomeConfigurator.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit
import DataServices

public class HomeConfigurator{
    public static var instance = HomeConfigurator()
    
    public func createMainView() -> UIViewController{
        let service = NewsService()
        
        let interactor : HomeInteractorProtocol = HomeInteractor(newsService: service)
        let preseenter : HomePresenterProtocol & HomeInteractorOutputProtocol = HomePresenter(interactor: interactor)
        let view  : UIViewController & HomeViewProtocol = HomeViewController(presenter: preseenter)
        let homeRouter : HomeRouterProtocol = HomeRouter(viewcontroller: view)
        
        view.presenter = preseenter
        preseenter.view = view
        preseenter.router = homeRouter
        preseenter.interactor = interactor
        
        return view
    }
}
