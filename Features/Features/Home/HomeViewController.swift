//
//  HomeViewController.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import DataServices

class HomeViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var presenter: HomePresenterProtocol?
    var categoryList = ["Business", "Entertainment", "General", "Health", "Science", "Sports", "Technology"]
    var className: String = String(describing: HomeViewController.self)
    
    var newsList : [Article] = []
    var search = ""
    
    init(presenter : HomePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: className, bundle: Bundle(for: HomeViewController.self))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.searchTextField.textColor = .white
        
        homeTableView.register(CategoryTableViewCell.nib(), forCellReuseIdentifier: CategoryTableViewCell.identifier)
        homeTableView.register(HeadlineTableViewCell.nib(), forCellReuseIdentifier: HeadlineTableViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.getHeadlines(page: 1, pageSize: 5)
    }
    
}


extension HomeViewController : HomeViewProtocol{
    func showHeadlines(result: NewsResponse) {
        newsList = result.articles!
        homeTableView.reloadData()
    }
    
    func showProgress() {
        indicator.startAnimating()
        indicator.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    func hideProgress() {
        indicator.stopAnimating()
        indicator.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: HeadlineTableViewCell.identifier, for: indexPath) as! HeadlineTableViewCell
            cell.setNewsList(list: newsList)
            cell.presenter = presenter
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.identifier, for: indexPath) as! CategoryTableViewCell
            cell.setData(name: categoryList[indexPath.row - 1])
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != 0 else { return }
        presenter?.getSelectedCategory(category: categoryList[indexPath.row - 1])
    }
    
}

extension HomeViewController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter?.getNewsSearch(search: search)
    }
}
