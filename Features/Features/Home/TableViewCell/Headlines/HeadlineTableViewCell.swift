//
//  HeadlineTableViewCell.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import DataServices

class HeadlineTableViewCell: UITableViewCell {

    @IBOutlet weak var headlineCollectionView: UICollectionView!
    
    weak var presenter : HomePresenterProtocol?
    
    var newsList : [Article] = []
    static let identifier = "HeadlineTableViewCell"
    static func nib()->UINib{
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        headlineCollectionView.register(HeadLineCollectionViewCell.nib(), forCellWithReuseIdentifier: HeadLineCollectionViewCell.identifier)
        
        headlineCollectionView.delegate = self
        headlineCollectionView.dataSource = self
       
        
        
    }
    
    func setNewsList(list : [Article]){
        newsList = list
        headlineCollectionView.reloadData()
    }
    
    @IBAction func showMoreClicked(_ sender: Any) {
        presenter?.getMoreHeadlines()
    }
}

extension HeadlineTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HeadLineCollectionViewCell.identifier, for: indexPath) as! HeadLineCollectionViewCell
        cell.setDataHeadline(title: newsList[indexPath.row].title ?? "", desc: newsList[indexPath.row].articleDescription ?? "", img: newsList[indexPath.row].urlToImage ?? ApiEndPoint.baseURL.rawValue)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 1.2

        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.getSelectedHeadlines(url: newsList[indexPath.row].url!)
    }
}
