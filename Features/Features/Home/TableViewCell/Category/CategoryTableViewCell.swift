//
//  CategoryTableViewCell.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImgView: UIImageView!
    
    static let identifier = "CategoryTableViewCell"
    
    static func nib()->UINib{
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(name : String) {
        
        categoryName.text = name
        categoryImgView.image = UIImage(named: name)
    }
    
}
