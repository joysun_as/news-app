//
//  HomeInteractor.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import Foundation
import DataServices

class HomeInteractor : HomeInteractorProtocol{
    weak var interactorOutput: HomeInteractorOutputProtocol?
    
    var service: NewsServiceProtocol?
    
    init(newsService : NewsServiceProtocol) {
        service = newsService
    }
    
    func fetchHeadlines(page: Int, pageSize: Int) {
        service?.getNewsHeadlineList(page: page, pageSize: pageSize){result in
            self.interactorOutput?.completeFetchHeadlines(result: result)
        }
    }
    
    
}
