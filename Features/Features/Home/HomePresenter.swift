//
//  HomePresenter.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import Foundation
import DataServices

class HomePresenter: HomePresenterProtocol {
    
   weak var view: HomeViewProtocol?
    
    var interactor: HomeInteractorProtocol?
    
    var router: HomeRouterProtocol?
    
    init(interactor : HomeInteractorProtocol) {
        self.interactor = interactor
    }
    
    func getHeadlines(page: Int, pageSize: Int) {
        view?.showProgress()
        interactor?.interactorOutput = self
        interactor?.fetchHeadlines(page: page, pageSize: pageSize)
    }
    
    func getSelectedCategory(category: String) {
        router?.navToSourceList(category: category)
    }
    
    func getSelectedHeadlines(url: String) {
        router?.navToDetailNews(url: url)
    }
    
    func getNewsSearch(search: String) {
        router?.navToNewsList(type : ListType.search, identifier: search)
    }
    
    func getMoreHeadlines() {
        router?.navToNewsList(type: ListType.headline, identifier: "Headlines")
    }
    
    
    
}

extension HomePresenter : HomeInteractorOutputProtocol{
    func completeFetchHeadlines(result: NewsResponse) {
        view?.hideProgress()
        view?.showHeadlines(result: result)
    }
    
}
