//
//  HeadLineCollectionViewCell.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import AlamofireImage

class HeadLineCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var headlineImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    static let identifier = "HeadLineCollectionViewCell"
    
    static func nib()->UINib{
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataHeadline(title : String, desc : String, img : String) {
        titleLabel.text = title
        descLabel.text = desc
        headlineImg.af.setImage(withURL: URL(string: img)! , placeholderImage: UIImage(named: "news_placeholder"))
    }

}
