//
//  NewsListInteractor.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

class NewsListInteractor: NewsListInteractorProtocol {
    var service: NewsServiceProtocol?
    weak var interactorOutput: NewsListInteractorOutputProtocol?
    
    init(newsService : NewsServiceProtocol) {
        service = newsService
    }
    
    func fetchNewsBySource(source: String, page: Int, pageSize: Int) {
        service?.getNewsBySource(page: page, pageSize: pageSize, source: source){ result in
            self.interactorOutput?.completeFetchNews(result: result)
        }
    }
    
    func fetchNewsBySearch(search: String, page: Int, pageSize: Int) {
        service?.searchNews(page: page, pageSize: pageSize, search: search){result in
            self.interactorOutput?.completeFetchNews(result: result)
        }
    }
    
    func fetchNewsByHeadlines(page: Int, pageSize: Int) {
        service?.getNewsHeadlineList(page: page, pageSize: pageSize){ result in
            self.interactorOutput?.completeFetchNews(result: result)
            
        }
    }
    

}
