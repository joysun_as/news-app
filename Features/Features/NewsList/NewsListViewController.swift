//
//  NewsListViewController.swift
//  Features
//
//  Created by DNR on 26/03/22.
//

import UIKit
import DataServices

class NewsListViewController: UIViewController {
    

    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    var presenter: NewsListPresenterProtocol?
    var className: String = String(describing: NewsListViewController.self)
    var newsList :[Article] = []
    var page : Int = 1
    var pageLimit : Int = 20
    
    var selectedType : ListType?
    var identifier : String?
    
    
    init(presenter : NewsListPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: className, bundle: Bundle(for: NewsListViewController.self))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsCollectionView.register(HeadLineCollectionViewCell.nib(), forCellWithReuseIdentifier: HeadLineCollectionViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch selectedType {
        case .search:
            presenter?.getNewsBySearch(search: identifier ?? "", page: page, pageSize: pageLimit, firstLoad: true)
            break
        case .headline :
            presenter?.getHeadlinesNews(page: page, pageSize: pageLimit, firstLoad: true)
            break
        
        case .source:
            presenter?.getNewsBySource(source: identifier ?? "", page: page, pageSize: pageLimit, firstLoad: true)
            break
        case .none:
            break
        }
    }

}

extension NewsListViewController : NewsListViewProtocol{
    func showNewsList(news: NewsResponse, firstLoad: Bool) {
        guard firstLoad else {
            newsList.append(contentsOf: news.articles!)
            newsCollectionView.reloadData()
            return
        }
        newsList = news.articles!
        newsCollectionView.reloadData()
    }
    
    func showProgress() {
        indicator.startAnimating()
        indicator.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    func hideProgress() {
        indicator.stopAnimating()
        indicator.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
}

extension NewsListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        newsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == newsList.count-1 {
            page+=1
            switch selectedType {
            case .search:
                presenter?.getNewsBySearch(search: identifier ?? "", page: page, pageSize: pageLimit, firstLoad: false)
                break
            case .headline :
                break
            case .source:
                presenter?.getNewsBySource(source: identifier ?? "", page: page, pageSize: pageLimit, firstLoad: false)
                break
            case .none:
                break
           
            }
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HeadLineCollectionViewCell.identifier, for: indexPath) as! HeadLineCollectionViewCell
        cell.setDataHeadline(title: newsList[indexPath.row].title ?? "", desc: newsList[indexPath.row].articleDescription ?? "", img: newsList[indexPath.row].urlToImage ?? ApiEndPoint.baseURL.rawValue)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.height / 3.2

        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.getSelectedNews(url: newsList[indexPath.row].url ?? "")
    }
    
    
}

