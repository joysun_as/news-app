//
//  NewsListPresenter.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

class NewsListPresenter: NewsListPresenterProtocol {
    var view: NewsListViewProtocol?
    var interactor: NewsListInteractorProtocol?
    var router: NewsListRouterProtocol?
    var firstLoad : Bool?
    
    init(interactor : NewsListInteractorProtocol) {
        self.interactor = interactor
    }
    
    func getNewsBySource(source: String, page: Int, pageSize: Int, firstLoad : Bool) {
        view?.showProgress()
        self.firstLoad = firstLoad
        interactor?.interactorOutput = self
        interactor?.fetchNewsBySource(source: source, page: page, pageSize: pageSize)
    }
    
    func getNewsBySearch(search: String, page: Int, pageSize: Int, firstLoad : Bool) {
        view?.showProgress()
        self.firstLoad = firstLoad
        interactor?.interactorOutput = self
        interactor?.fetchNewsBySearch(search: search, page: page, pageSize: pageSize)
    }
    
    func getHeadlinesNews(page: Int, pageSize: Int, firstLoad: Bool) {
        view?.showProgress()
        self.firstLoad = firstLoad
        interactor?.interactorOutput = self
        interactor?.fetchNewsByHeadlines(page: page, pageSize: pageSize)
    }
    
    func getSelectedNews(url: String) {
        router?.navToDetailNews(url: url)
    }
    
}

extension NewsListPresenter : NewsListInteractorOutputProtocol{
    func completeFetchNews(result: NewsResponse) {
        view?.hideProgress()
        view?.showNewsList(news: result, firstLoad : self.firstLoad!)
    }
    
}
