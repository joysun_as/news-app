//
//  NewsListRouter.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit
import DataServices

class NewsListRouter: NewsListRouterProtocol {
    
    let newsListVC : UIViewController
    
    init(viewcontroller : UIViewController) {
        newsListVC = viewcontroller
    }
    
    static func initModule() -> NewsListViewController {
        let service = NewsService()
        
        let interactor : NewsListInteractorProtocol = NewsListInteractor(newsService : service)
        let preseenter : NewsListPresenterProtocol & NewsListInteractorOutputProtocol = NewsListPresenter(interactor: interactor)
        let view  : UIViewController & NewsListViewProtocol = NewsListViewController(presenter: preseenter)
        let router : NewsListRouterProtocol = NewsListRouter(viewcontroller: view)
        
        view.presenter = preseenter
        preseenter.view = view
        preseenter.router = router
        preseenter.interactor = interactor
        
        return view as! NewsListViewController
    }
    
    func navToDetailNews(url: String) {
        let detailVC = DetailNewsViewController(nibName: "DetailNewsViewController", bundle: Bundle(for: DetailNewsViewController.self))
        detailVC.url = url
        newsListVC.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
}
