//
//  NewsListProtocols.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

protocol NewsListViewProtocol : AnyObject {
    var presenter : NewsListPresenterProtocol? {get set}
    
    func showNewsList(news : NewsResponse, firstLoad : Bool)
    func showProgress()
    func hideProgress()
    
}

protocol NewsListPresenterProtocol : AnyObject {
    var view : NewsListViewProtocol? {get set}
    var interactor : NewsListInteractorProtocol? {get set}
    var router : NewsListRouterProtocol? {get set}
    var firstLoad : Bool? {get set}
    
    func getNewsBySource(source : String, page : Int, pageSize : Int, firstLoad : Bool)
    func getNewsBySearch(search : String, page : Int, pageSize : Int, firstLoad : Bool)
    func getHeadlinesNews(page : Int, pageSize : Int, firstLoad : Bool)
    func getSelectedNews(url : String)
}

protocol NewsListInteractorProtocol : AnyObject {
    var service : NewsServiceProtocol? {get set}
    var interactorOutput : NewsListInteractorOutputProtocol? {get set}
    
    func fetchNewsBySource(source : String, page : Int, pageSize : Int)
    func fetchNewsBySearch(search : String, page : Int, pageSize : Int)
    func fetchNewsByHeadlines (page : Int, pageSize : Int)
    
}

protocol NewsListInteractorOutputProtocol : AnyObject {
    func completeFetchNews(result : NewsResponse)
}

protocol NewsListRouterProtocol : AnyObject {
    static func initModule()->NewsListViewController
    func navToDetailNews(url : String)
}
