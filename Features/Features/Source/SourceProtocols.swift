//
//  SourceProtocols.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

protocol SourceViewProtocol : AnyObject {
    var presenter : SourcePresenterProtocol? {get set}
    
    func showSource(result : [Source], firstLoad : Bool)
    func showProgress()
    func hideProgress()
    
}

protocol SourcePresenterProtocol : AnyObject {
    var view : SourceViewProtocol? {get set}
    var interactor : SourceInteractorProtcol? {get set}
    var router : SourceRouterProtocol? {get set}
    var firstLoad : Bool? {get set}
    
    func getSource(category : String, page : Int, pageSize : Int, firstLoad : Bool)
    func getNewsBySource(identifier : String)
    
}

protocol SourceInteractorProtcol : AnyObject {
    var service : NewsServiceProtocol? {get set}
    var interactorOutput : SourceInteractorOutputProtocol? {get set}
    
    func fetchSourceListByCategory(category : String, page : Int, pageSize : Int)
}

protocol SourceInteractorOutputProtocol : AnyObject {
    func completeFetchSource(source : [Source])
   
}

protocol SourceRouterProtocol:AnyObject {
    static func initModule()->SourceListViewController
    func navToNewsList(type : ListType, identifier : String)
}
