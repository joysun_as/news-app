//
//  SourceListViewController.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit
import DataServices

class SourceListViewController: UIViewController {

    @IBOutlet weak var sourceTableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var presenter: SourcePresenterProtocol?
    var className: String = String(describing: SourceListViewController.self)
    var sourceList : [Source] = []
    var category = ""
    var page = 1
    var pageSize = 20
    
    init(presenter : SourcePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: className, bundle: Bundle(for: SourceListViewController.self))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sourceTableView.register(SourceTableViewCell.nib(), forCellReuseIdentifier: SourceTableViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        page = 1
        presenter?.getSource(category: category, page: page, pageSize: pageSize, firstLoad: true)
    }
}

extension SourceListViewController : SourceViewProtocol{
    func showSource(result: [Source], firstLoad : Bool) {
        guard firstLoad else {
            sourceList.append(contentsOf: result)
            sourceTableView.reloadData()
            return
        }
        sourceList = result
        sourceTableView.reloadData()
        
    }
    
    func showProgress() {
        indicator.startAnimating()
        indicator.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    func hideProgress() {
        indicator.stopAnimating()
        indicator.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
}

extension SourceListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SourceTableViewCell.identifier) as! SourceTableViewCell
        cell.setData(source: sourceList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.getNewsBySource(identifier: sourceList[indexPath.row].id ?? "")
    }
    
}
