//
//  SourceTableViewCell.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit
import DataServices

class SourceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    
    static let identifier = "SourceTableViewCell"
    static func nib()->UINib{
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(source : Source){
        sourceLabel.text = source.name
        descLabel.text = source.sourceDescription
    }
    
}
