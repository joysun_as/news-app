//
//  SourcePresenter.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

class SourcePresenter : SourcePresenterProtocol {
    weak var view: SourceViewProtocol?
    var interactor: SourceInteractorProtcol?
    var router: SourceRouterProtocol?
    var firstLoad: Bool?
    
    init(interactor : SourceInteractorProtcol) {
        self.interactor = interactor
    }
    
    func getSource(category: String, page: Int, pageSize: Int, firstLoad : Bool) {
        view?.showProgress()
        self.firstLoad = firstLoad
        interactor?.interactorOutput = self
        interactor?.fetchSourceListByCategory(category: category, page: page, pageSize: pageSize)
    }
    
    func getNewsBySource(identifier: String) {
        router?.navToNewsList(type: ListType.source, identifier: identifier)
    }
    
    
}

extension SourcePresenter : SourceInteractorOutputProtocol{
    func completeFetchSource(source: [Source]) {
        view?.hideProgress()
        view?.showSource(result: source, firstLoad: self.firstLoad!)
    }
    
    
}
