//
//  SouuceRouter.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import UIKit
import DataServices

class SourceRouter : SourceRouterProtocol{
    let sourceVC : UIViewController
    
    init(viewcontroller : UIViewController) {
        sourceVC = viewcontroller
    }
    
    static func initModule() -> SourceListViewController {
        let service = NewsService()
        
        let interactor : SourceInteractorProtcol = SourceInteractor(service : service)
        let preseenter : SourcePresenterProtocol & SourceInteractorOutputProtocol = SourcePresenter(interactor: interactor)
        let view  : UIViewController & SourceViewProtocol = SourceListViewController(presenter: preseenter)
        let router : SourceRouterProtocol = SourceRouter(viewcontroller: view)
        
        view.presenter = preseenter
        preseenter.view = view
        preseenter.router = router
        preseenter.interactor = interactor
        
        return view as! SourceListViewController
    }
    
    func navToNewsList(type: ListType, identifier: String) {
        let newsListVC = NewsListRouter.initModule()
        newsListVC.selectedType = .source
        newsListVC.identifier = identifier
        newsListVC.navigationItem.title = identifier
        sourceVC.navigationController?.pushViewController(newsListVC, animated: true)
    }
    
    
}
