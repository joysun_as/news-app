//
//  SourceInterractor.swift
//  Features
//
//  Created by DNR on 27/03/22.
//

import Foundation
import DataServices

class SourceInteractor : SourceInteractorProtcol {
    var service: NewsServiceProtocol?
    weak var interactorOutput: SourceInteractorOutputProtocol?
    
    init(service : NewsServiceProtocol) {
        self.service = service
    }
    
    func fetchSourceListByCategory(category: String, page: Int, pageSize: Int) {
        service?.getSourceList(page: page, pageSize: pageSize, category: category){ result in
            self.interactorOutput?.completeFetchSource(source: result)
            
        }
    }
    
    
}
